﻿using RSAExtensions;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Windows;

namespace RSAKeyGenerate
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            TbxFilePath.Text = Directory.GetCurrentDirectory();
        }

        private void BtnGenerate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var filePath = TbxFilePath.Text;

                if (CbxPkcs1.IsChecked != true && CbxPkcs8.IsChecked != true)
                {
                    MessageBox.Show("请选择要生成的密钥格式");
                    return;
                }

                if (string.IsNullOrWhiteSpace(filePath))
                {
                    MessageBox.Show("请选择输出路径");
                    return;
                }

                if (CbxPkcs1.IsChecked == true && CbxPkcs8.IsChecked != true)
                {
                    GenerateAndSaveKeyPKCS1(filePath, RbtnIsPEM.IsChecked == true);
                }
                else if (CbxPkcs1.IsChecked != true && CbxPkcs8.IsChecked == true)
                {
                    GenerateAndSaveKeyPKCS8(filePath, RbtnIsPEM.IsChecked == true);
                }
                else if (CbxPkcs1.IsChecked == true && CbxPkcs8.IsChecked == true)
                {
                    GenerateAndSaveKey(filePath, RbtnIsPEM.IsChecked == true);
                }

                MessageBox.Show("密钥对生成成功");
            }
            catch (Exception ex)
            {
                MessageBox.Show("程序异常：" + ex.Message);
            }
        }

        private RSA GenerateAndSaveKey(string filePath, bool isPem)
        {
            string publicKeys, privateKeys, privateKeys_pkcs8, publicKeys_pkcs8, ext;

            ext = isPem ? "pem" : "txt";

            var rsa = RSA.Create();

            privateKeys = rsa.ExportPrivateKey(RSAKeyType.Pkcs1, isPem); //私钥
            publicKeys = rsa.ExportPublicKey(RSAKeyType.Pkcs1, isPem);   //公钥
            privateKeys_pkcs8 = rsa.ExportPrivateKey(RSAKeyType.Pkcs8, isPem); //私钥
            publicKeys_pkcs8 = rsa.ExportPublicKey(RSAKeyType.Pkcs8, isPem);   //公钥

            File.WriteAllText(System.IO.Path.Combine(filePath, $"key.pkcs8.{ext}"), privateKeys_pkcs8);
            File.WriteAllText(System.IO.Path.Combine(filePath, $"key.public.pkcs8.{ext}"), publicKeys_pkcs8);
            File.WriteAllText(System.IO.Path.Combine(filePath, $"key.{ext}"), privateKeys);
            File.WriteAllText(System.IO.Path.Combine(filePath, $"key.public.{ext}"), publicKeys);

            return rsa;
        }

        private RSA GenerateAndSaveKeyPKCS8(string filePath, bool isPem)
        {
            string publicKeys, privateKeys, ext;

            ext = isPem ? "pem" : "txt";

            var rsa = RSA.Create();

            privateKeys = rsa.ExportPrivateKey(RSAKeyType.Pkcs8, isPem); //私钥
            publicKeys = rsa.ExportPublicKey(RSAKeyType.Pkcs8, isPem);   //公钥

            File.WriteAllText(System.IO.Path.Combine(filePath, $"key.pkcs8.{ext}"), privateKeys);
            File.WriteAllText(System.IO.Path.Combine(filePath, $"key.public.pkcs8.{ext}"), publicKeys);

            return rsa;
        }

        private RSA GenerateAndSaveKeyPKCS1(string filePath, bool isPem)
        {
            string publicKeys, privateKeys, ext;

            ext = isPem ? "pem" : "txt";

            var rsa = RSA.Create();

            privateKeys = rsa.ExportPrivateKey(RSAKeyType.Pkcs1, isPem); //私钥
            publicKeys = rsa.ExportPublicKey(RSAKeyType.Pkcs1, isPem);   //公钥

            File.WriteAllText(System.IO.Path.Combine(filePath, $"key.{ext}"), privateKeys);
            File.WriteAllText(System.IO.Path.Combine(filePath, $"key.public.{ext}"), publicKeys);

            return rsa;
        }

        private void BtnChooseFilePath_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            folderBrowserDialog.SelectedPath = TbxFilePath.Text;
            folderBrowserDialog.ShowNewFolderButton = true;
            folderBrowserDialog.Description = "选择输出路径";
            folderBrowserDialog.UseDescriptionForTitle = true;
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TbxFilePath.Text = folderBrowserDialog.SelectedPath;
            }
        }
    }
}
